const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {

	console.log(req.body);


	let newCourse = new Course({
	
	name: req.body.name,
	description: req.body.description,
	price: req.body.price,
	});


	newCourse.save()
	.then(user => res.send(user))
	.catch(error => res.send(error))

};

module.exports.getAllCourses = (req, res) => {
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

//Activity 3

module.exports.getSingleCourse = (req, res) => {
	console.log(req.params);
	Course.findById(req.params.id)

	.then(result => res.send(result))
	.catch(error => res.send(error))

};

//Activity 4

module.exports.courseArchive = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedArchive => res.send(updatedArchive))
	.catch(error => res.send(error))


}

module.exports.courseActivate = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedActivate => res.send(updatedActivate))
	.catch(error => res.send(error))


}

module.exports.getActiveCourses = (req, res) => {
	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

module.exports.updateCourse = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

module.exports.getInactiveCourse = (req, res) => {
	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

module.exports.findCoursesByName = (req, res) => {
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {
		if(result.length === 0){
			return res.send("No courses found")
		}else{
			return res.send(result)
		}
	})
	.catch(error => res.send(error))
}

module.exports.findCoursesByPrice = (req, res) => {
	Course.find({price: req.body.price})
	.then(result => {
		if(result == 0 ){
			return res.send("No course found")
		}else {
			return res.send(result)
		}
	})
	.catch(error => res.send(error))
}

module.exports.getEnrollees = (req, res) => {
		console.log(req.params.id);
		Course.findById(req.params.id)
		.then(course => res.send(course.enrollees))
	
	/*.then(course => {
	if(course.enrollees.length === 0 ){
		return res.send("No students enrolled found")
	}else {
		return res.send(course.enrollee)
	}
	})*/
	.catch(error => res.send(error))
}


