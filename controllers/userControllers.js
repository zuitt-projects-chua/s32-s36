//import user model
//import bcrypt module
const bcrypt = require("bcrypt")

const User = require("../models/User");

const Course = require("../models/Course")

const auth = require("../auth")

//Controller
//user registration

module.exports.registerUser = (req, res) => {

	console.log(req.body);

/*
bcrypt adds a layer of security to your users password
hash our password to a randomized character version of the original string

bcrypt.hashSync(<stringtobehash>, <rounds>)

Salt-rounds are the number of items the charcaters in the hash are randomized
*/

	const hashedPW = bcrypt.hashSync(req.body.password, 10)

	let newUser = new User({
	
	firstName: req.body.firstName,
	lastName: req.body.lastName,
	email: req.body.email,
	mobileNo: req.body.mobileNo,
	password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error))

};

//Create new user document

//Retrieve all user

module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

//Login

module.exports.loginUser = (req, res) => {
	//to make sure if the req.body being input pushes through, it will display console log in the terminal that the iinput is process
	console.log(req.body);
	/*
	find user by email
	if user email is found, check the password
	if email not found send a message
	checking password is same, we will generate a key to access our app
	if not, we ill turn him away by sending a messagge to the client
	*/
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("User does not exist")
		}else {
			//bcrypt returns a boolean value
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			//truthy 
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			}else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(error => res.send(error))
}

//Get use Details

module.exports.getUserDetails = (req, res) => {
	console.log(req.user);
	/*
	1 find a logged in user's document from our db and sent it to the client by its id
	*/
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))


}


//Activity 3
module.exports.checkEmailExists = ( req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
	.then(result => {
		if(result !== null && result.name === req.body.username){
			return res.send(`The email ${req.body.email} found and already registered. Please use new email`)
		} else {
			return res.send(`Email ${req.body.email} is available`)

		}
	})
	.catch(error => res.send(error))


}

module.exports.updateAdmin = (req, res) => {
	//this will change the target user
	console.log(req.params.id);
	
	/*//this will change the login user 
	console.log(req.user.id)
*/
	let updates = {
		isAdmin: true
	}

	/*
	where will you get the id,
	what is the uppdates
	{new true} latest version
	*/
	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))


}

module.exports.updateUserDetails = (req, res) => {
	console.log(req.body);
	//check the user id
	console.log(req.user.id);

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))



}

//Enroll A user

module.exports.enroll = async (req,res) => {
	//enrollment process
	/*
		-look for the user by id
		push the details of the course to enroll in
		well push to a new enrollment subdocument in our user
		-look for the course by its id
		push the details of the enrollee whos trying to enroll
		well push to a new enrollment subdocument in our course
		when both saving documents are sucessful, send a message to a client
	*/

	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	} 
	/*
	find the user:
	async allows us to make our function asynchronous, instead of js regular behavior of running each code by line, it will allow us to wait for the result of the function

	await allows us to wait for the result of the function before proceeding

	it will make two functions at same time , the last if, two function await each other to process the ondition

	*/
	let isUserUpdated = await User.findById(req.user.id)
		.then(user => {
			console.log(user)
			let newEnrollment = {
				courseId: req.body.courseId
			}
			user.enrollments.push(newEnrollment)
			return user.save()
			.then(user => true)
			.catch(error => error.message)
		})

		/*
			ifuser does not contain boolean value of true, stop the process and send message to client
		*/

		if (isUserUpdated !== true) {
			return res.send({message: isUserUpdated})
		}

		let isCourseUpdated = await Course.findById(req.body.courseId)
			.then(course => {
				console.log(course)
				let enrollee = {
					userID: req.user.id
				}
				course.enrollees.push(enrollee)
				return course.save()
				.then(course => true)
				.catch(error => error.message)
			})
			if(isCourseUpdated !== true){
				return res.send({message: isCourseUpdated})
			}
			if(isUserUpdated && isCourseUpdated){
				return res.send({message: 'Enrolled Successfully'})
			}


	}

module.exports.getEnrollments = (req, res) => {
	console.log(req.user);
	/*
	1 find a logged in user's document from our db and sent it to the client by its id
	*/
	User.findById(req.user.id)
	.then(course => res.send(course.enrollments))
	/*.then(course => {
	if(course.enrollments.length === 0 ){
		return res.send("No students enrolled found")
	}else {
		return res.send(course.enrollments)
	}
	})*/
	.catch(error => res.send(error))


}





