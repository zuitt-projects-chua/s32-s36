const express = require('express');

const mongoose = require('mongoose');

const cors = require('cors');
//allow our server to be available to our front end application

const userRoutes = require('./routes/userRoutes')

const courseRoutes = require('./routes/courseRoutes')

const app = express();
//initially create database but only 
mongoose.connect("mongodb+srv://admin_chua:batch169@batch169chua.g8jtg.mongodb.net/bookingAPI169?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));

const port = 4000;

app.use(express.json());

app.use(cors());

//endpoint users, all routes in user routes will automaticaly start with /users
app.use('/users', userRoutes);

app.use('/courses', courseRoutes);

app.listen(port, () => console.log (`Server is now running at port ${port}`))