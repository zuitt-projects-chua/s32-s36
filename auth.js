
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

/*
Notes 

JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application, 
allows us to authorize our users to access or disaalow access to certain parts of our app

Jwt is like a gift wrapping service that is able to encod eour usr details which can only be unwrapped by jwt's own methods and if the secret provided is intact

if jwt seemed tampered with, we will reject the user attempt to access a feature

*/

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
return jwt.sign(data, secret, {/*option*/})
}

/*
You can only get a unique jwt with our secret if you log in to our app with the correct email and password

You can only get your own details from your own token from logging in 

jwt is not meant to store sensitive data
minimum viable product, we add the email, isadmin details of the logged in.

limit to id, and for every route and feature, you can simply look up for user in the databse to get his details

We will verify the legitimacy of the jwt everytime a user access a restricted feature
each jwt contains a secret only a server knows
Reject if :
changed in any way
secret is different
does not contain secret
*/

module.exports.verify = (req, res, next) => {
//contains sensitive data and especially token
	let token = req.headers.authorization
	console.log(token)
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})
	} else {
		token = token.slice(7, token.length)
		//Bearer uiedhuihsfhff
		jwt.verify(token, secret, (err, decodedToken) => {
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				req.user = decodedToken;
				next()
			}
		})
	}
};

module.exports.verifyAdmin = (req, res, next) => {

	if (req.user.isAdmin){
		next()
	} else {
		return res.send({
			auth: "Failed",
			message: "Forbidden Action"
		})
	}

}


