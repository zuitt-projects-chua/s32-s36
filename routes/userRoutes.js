const express = require('express');

const router = express.Router();

//import user controller
const userControllers = require("../controllers/userControllers")

const auth = require("../auth");

const {verify, verifyAdmin} = auth;
//Routes
//user registration
router.post('/', userControllers.registerUser)
//Retrieve all users
router.get('/', userControllers.getAllUsers)
//Login route
router.post('/login', userControllers.loginUser)
//retrieve user details
router.get('/getUserDetails', verify, userControllers.getUserDetails)

//Activity 3
router.post('/checkEmailExists', userControllers.checkEmailExists)

//update to admin
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin)

router.put("/updateUserDetails", verify, userControllers.updateUserDetails)

//enrol registered user

router.post("/enroll", verify, userControllers.enroll);

router.get("/getEnrollments", verify, userControllers.getEnrollments)




module.exports = router;

